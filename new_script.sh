#!/usr/bin/bash
# number of Nodes
#SBATCH -N 1
# Queue where the job will be submitted
#SBATCH -p shareq
# NUmber of CPU cores per node
#SBATCH -n 12
# Memory per node dataset + model + training variables
#SBATCH --mem 20G
# Estimated time 
#SBATCH -t 2:00:00
# Number of GPU per node
#SBATCH --gres=gpu:1
# Name will appear in the queue
#SBATCH --job-name=CNN_TEST
# Main output file
#SBATCH --output=new_output.txt
#SBATCH --error=new_error.err
# What messages are we waiting
#SBATCH --mail-type=ALL
# Email addres
#SBATCH --mail-user=f.cruciani@ulster.ac.uk

module purge
module load shared
module load cm-ml-python3deps/2.3.2 
module load ml-python3/tensorflow/1.14.0 
module load ml-python3/keras/2.2.4 
module load openmpi/cuda/64/3.1.4
module load hpcx/2.4.0
module list

python3.6 train_128_filters_resampling_3D.py

