import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, accuracy_score
from sklearn.utils import class_weight
#local imports
import utils
import Classifiers
import HAPT_Dataset as UCI_HAPT
from HAPT_Dataset import ucihapt_datapath
from os.path import expanduser
import sys


#get actual home path for current user
home = expanduser("~")


#args_list = str(sys.argv)
n_layers = int(sys.argv[1])
k = int(sys.argv[2])
nfilters = int(sys.argv[3])

if (n_layers in [1,2,3,4]) and (k in [2,8,16,24,32,64]) and (nfilters in [12,24,48,96,128]):
	
	#Number of cores used for parallel loading
	num_threads = 5

	print("Loading test set")
	test_uuids = UCI_HAPT.get_test_uuids()
	(X_test,y_test) = UCI_HAPT.get_all_data_multi_thread_noresampling_3D(test_uuids,num_threads)

	classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LAYING","TRANSITION"]


	clf = Classifiers.IMU_CNN_3D_FEATURE_EXTRACTOR(patience=1000,num_filters=nfilters,layers=n_layers,kern_size=k,divide_kernel_size=True,suffix="50Hz")
	
	#Loading saved model
	clf.loadBestWeights()
	y_true = [ [np.argmax(y)] for y in y_test]#one_hot(labels_test)

	y_predictions = clf.predict(X_test,batch_size=1)
	y_predictions_inv = [ [np.argmax(y)] for y in y_predictions]

	utils.printClassificationReport(true=y_true,predictions=y_predictions_inv,classes=classes,filename="./results_paper/final_IMU_3D_"+str(n_layers)+"-CNN_k"+str(k)+"_"+str(nfilters)+"_filters_classification_report.txt")
	utils.plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/final_IMU_3D_"+str(n_layers)+"-CNN_k"+str(k)+"_"+str(nfilters)+"_filters_ConfusionMatrix_normalized.png")
	utils.printBalancedAccuracyScore(true=y_true,pred=y_predictions_inv,filename="./results_paper/final_IMU_3D_"+str(n_layers)+"-CNN_k"+str(k)+"_"+str(nfilters)+"_filters_balanced_accuracy.txt")
	utils.plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/final_IMU_3D_"+str(n_layers)+"-CNN_k"+str(k)+"_"+str(nfilters)+"_filters_ConfusionMatrix.png",normalize=False)

else:
	print("Not valid argument: n-layers kernel_size num_filters")