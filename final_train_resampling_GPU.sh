#!/usr/bin/bash
#SBATCH -N 1
#SBATCH -p shareq
#SBATCH -n 21
#SBATCH --mem 200G
#SBATCH -t 08:10:00
#SBATCH --gres=gpu:1
#SBATCH --job-name=CNN_filters
#SBATCH --output=output_filters.txt
#SBATCH --error=error_filters.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=f.cruciani@ulster.ac.uk

module purge
module load shared
module load cm-ml-python3deps/2.3.2 
module load ml-python3/tensorflow/1.14.0 
module load ml-python3/keras/2.2.4 
module load openmpi/cuda/64/3.1.4
module load hpcx/2.4.0
module list
#layer k filters
python3.6 final_train_CNN_resampling_3D.py 2 24 12
python3.6 final_train_CNN_resampling_3D.py 2 24 24
python3.6 final_train_CNN_resampling_3D.py 3 24 24


#srun gpu_test_5_fold_0.py