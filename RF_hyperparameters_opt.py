from sklearn.ensemble import RandomForestClassifier
import ES_Dataset as es
from sklearn.model_selection import RandomizedSearchCV,GridSearchCV# Number of trees in random forest
import Classifiers
import numpy as np
#Does not matter. It's going to load all users combining train and test
fold = "0"

train_uuids,test_uuids = es.get_train_test_uuids(fold,include_gyro=False)

#print("Training: ",train_uuids)
#print("Test: ",test_uuids)

print("Training A: ",len(train_uuids))
print("Test A: ",len(test_uuids))


train_uuids,test_uuids = es.get_train_test_uuids(fold,include_gyro=True)
print("Training A+G: ",len(train_uuids))
print("Test A+G: ",len(test_uuids))

#geting all users
train_uuids.extend(test_uuids)
print("Users for training: ",len(train_uuids))
X_train, y_train = es.get_all_dataset(train_uuids, show_progress=True,balanced=True,smote=True)
print("X: ",X_train.shape)
print("y: ",y_train.shape)


featExtractor = Classifiers.HAPT_IMU_CNN_NOBN(patience=250,layers=3,kern_size=32,divide_kernel_size=True)
featExtractor.loadBestWeights()
print("Optimizinf idle case")

X_auto_features = featExtractor.get_layer_output(X_train,"automatic_features")

y_true = []

for y in y_train:
	if y == 0 or y == 1:
		y_true.append(0)
	else:
		y_true.append(y-1)


n_estimators = [int(x) for x in np.linspace(start = 40, stop =70, num = 7)]
# Number of features to consider at every split
max_features = ['auto']#['auto', 'sqrt']
# Maximum number of levels in tree
max_depth = [int(x) for x in np.linspace(140, 150, num = 10)] #[int(x) for x in np.linspace(10, 250, num = 10)]
max_depth.append(None)
# Minimum number of samples required to split a node
min_samples_split = [2]#[2, 5, 10, 50]
# Minimum number of samples required at each leaf node
min_samples_leaf = [2]#[ 2, 5, 10]
# Method of selecting samples for training each tree
bootstrap = [True, False]# Create the random grid
param_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap}


print(param_grid)


# Use the random grid to search for best hyperparameters
# First create the base model to tune
rf = RandomForestClassifier()
# Random search of parameters, using 3 fold cross validation, 
# search across 100 different combinations, and use all available cores
#rf_random = RandomSearchCV(estimator = rf, param_distributions = random_grid, n_iter = 100, cv = 3, verbose=2, random_state=42, n_jobs = -1)# Fit the random search model
rf_random = GridSearchCV(estimator = rf, param_grid = param_grid, cv = 3, verbose=2, n_jobs = -1)# Fit the random search model
rf_random.fit(X_auto_features, y_true)

print("*****DONE!******")
print("Best parameters (IDLE case):")
print(rf_random.best_params_)


