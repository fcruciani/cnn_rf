import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, accuracy_score
from sklearn.utils import class_weight
#local imports
import Classifiers
import HAPT_Dataset as UCI_HAPT
from HAPT_Dataset import ucihapt_datapath
from os.path import expanduser


#get actual home path for current user
home = expanduser("~")




train_uuids = UCI_HAPT.get_train_uuids()
test_uuids = UCI_HAPT.get_test_uuids()

#tr_uuids = train_uuids[0:18]
#vl_uuids = train_uuids[18:21]

(X_train,y_train) = UCI_HAPT.get_all_data_multi_thread_resampling_3D(train_uuids,5)

#(X_vld,y_vld) = imu_bg.get_all_data(vl_uuids)
(X_test,y_test) = UCI_HAPT.get_all_data_multi_thread_resampling_3D(test_uuids,5)

classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LAYING","TRANSITION"]

sFilters = input("Number of filters e.g. 12-24-48-96-128:")

#if sFilters not in ["12","24","48","96","128"]:
#	input("Not valid number fo filters")

sFilters = "12"

clf = Classifiers.IMU_CNN_3D_FEATURE_EXTRACTOR(patience=2500,num_filters=int(sFilters),layers=3,kern_size=32,divide_kernel_size=True,suffix="40Hz")
#uuids,epochs, val_data, imu_bg):
#clf.fit_gen(uuids=tr_uuids,epochs=10000,val_data=(X_vld,y_vld),imu_bg=imu_bg)
#clf.fit(X_train,y_train,X_test,y_test,batch_size=32,epochs=10000)
#class_weights = {0:1.,1:1.,2:1.,3:1.,4:1.,5:1.,6:10.}

y_train_inv = [ np.argmax(y) for y in y_train]
class_weights = class_weight.compute_class_weight('balanced',np.unique(y_train_inv),y_train_inv)
d_class_weights = dict(enumerate(class_weights))
clf.fit_class_weights(X_train,y_train,X_test,y_test,batch_size=32,class_weight=d_class_weights,epochs=50000)



'''
#Loading saved model
clf.loadBestWeights()
y_true = [ [np.argmax(y)] for y in y_test]#one_hot(labels_test)

y_predictions = clf.predict(X_test,batch_size=1)
y_predictions_inv = [ [np.argmax(y)] for y in y_predictions]

printClassificationReport(true=y_true,predictions=y_predictions_inv,classes=classes,filename="IMU_CNN_classification_report.txt")
plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="IMU_CNN_ConfusionMatrix_normalized.png")
printBalancedAccuracyScore(true=y_true,pred=y_predictions_inv,filename="IMU_CNN_balanced_accuracy.txt")
plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="IMU_CNN_ConfusionMatrix.png",normalize=False)
'''