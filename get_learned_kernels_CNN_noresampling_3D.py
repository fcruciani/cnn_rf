import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, accuracy_score
from sklearn.utils import class_weight
#local imports
import utils
import Classifiers
import HAPT_Dataset as UCI_HAPT
from HAPT_Dataset import ucihapt_datapath
from os.path import expanduser
import sys


#get actual home path for current user
home = expanduser("~")


#args_list = str(sys.argv)
n_layers = int(sys.argv[1])
k = int(sys.argv[2])
nfilters = int(sys.argv[3])

if (n_layers in [1,2,3,4]) and (k in [2,8,16,24,32,64]) and (nfilters in [12,24,48,96,128]):
	
	classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LAYING","TRANSITION"]


	clf = Classifiers.IMU_CNN_3D_FEATURE_EXTRACTOR(patience=1000,num_filters=nfilters,layers=n_layers,kern_size=k,divide_kernel_size=True,suffix="50Hz")
	
	#Loading saved model
	clf.loadBestWeights()

	X_test = np.ones((1,128,8))

	kernels = clf.get_layer_output(X_test,"layer_1")

	print(kernels)
else:
	print("Not valid argument: n-layers kernel_size num_filters")