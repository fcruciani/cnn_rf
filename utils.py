from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, balanced_accuracy_score
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sn
import numpy as np
import pandas as pd 

#Setting Font size for the plot
font = {'family':'sans-serif', 'size':10}
matplotlib.rc('font',**font)


#Utility Functions
def printClassificationReport(predictions,true,classes,filename):
	cr = classification_report(np.array(true), np.array(predictions),target_names=classes,digits=4)
	print(cr)
	if not filename == "":
		with open(filename,"w") as out_file:
			out_file.write(cr)

def plotConfusionMatrix(predictions,true,classes,saveFig=True,showGraph=False,normalize=True,column=True,w=10,h=9,filename="undefined"):
		cm = confusion_matrix(np.array(true), np.array(predictions) )
		fig = plt.figure(figsize=(w, h))
		if normalize:
			if column:
				cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
			else:
				cm = cm.astype('float') / cm.sum(axis=0)[:, np.newaxis]
			df_cm = pd.DataFrame(cm,index=classes,columns=classes)
			ax = sn.heatmap(df_cm,cmap='Blues',annot=True)
		else:
			df_cm = pd.DataFrame(cm,index=classes,columns=classes)
			ax = sn.heatmap(df_cm,cmap='Blues',annot=True,fmt='d')
		plt.yticks(rotation=0)
		if showGraph:
			plt.show()
		if saveFig:
			fig.tight_layout()
			if filename == "undefined":
				fig.savefig(self.name+"_CM.png",dpi=300)
			else:
				fig.savefig(filename,dpi=300)

def printBalancedAccuracyScore(pred, true,filename=""):
		bal_acc = balanced_accuracy_score(np.array(true), np.array(pred))
		print("Balanced Accuracy: ",bal_acc)
		if not filename == "":
			with open(filename,"w") as out_file:
				out_file.write(str(bal_acc))



# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
	"""
	Call in a loop to create terminal progress bar
	@params:
		iteration   - Required  : current iteration (Int)
		total       - Required  : total iterations (Int)
		prefix      - Optional  : prefix string (Str)
		suffix      - Optional  : suffix string (Str)
		decimals    - Optional  : positive number of decimals in percent complete (Int)
		length      - Optional  : character length of bar (Int)
		fill        - Optional  : bar fill character (Str)
	"""
	percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
	filledLength = int(length * iteration // total)
	bar = fill * filledLength + '-' * (length - filledLength)
	print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
	# Print New Line on Complete
	if iteration == total:
		print()