import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, accuracy_score
from sklearn.utils import class_weight
#local imports
import Classifiers
import HAPT_Dataset as UCI_HAPT
from HAPT_Dataset import ucihapt_datapath
from os.path import expanduser


#get actual home path for current user
home = expanduser("~")

#Utility Functions
def printClassificationReport(predictions,true,classes,filename):
	cr = classification_report(np.array(true), np.array(predictions),target_names=classes,digits=4)
	print(cr)
	if not filename == "":
		with open(filename,"w") as out_file:
			out_file.write(cr)

def plotConfusionMatrix(predictions,true,classes,saveFig=True,showGraph=False,filename="undefined"):
		cm = confusion_matrix(np.array(true), np.array(predictions) )
		fig = plt.figure(figsize=(8, 7))
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
		df_cm = pd.DataFrame(cm,index=classes,columns=classes)
		ax = sn.heatmap(df_cm,cmap='Blues',annot=True)
		plt.yticks(rotation=0)
		if showGraph:
			plt.show()
		if saveFig:
			fig.tight_layout()
			if filename == "undefined":
				fig.savefig(self.name+"_CM.png",dpi=300)
			else:
				fig.savefig(filename,dpi=300)

def printBalancedAccuracyScore(pred, true,filename=""):
		bal_acc = balanced_accuracy_score(np.array(true), np.array(pred))
		print("Balanced Accuracy: ",acc)
		if not filename == "":
			with open(filename,"w") as out_file:
				out_file.write(str(acc))



train_uuids = UCI_HAPT.get_train_uuids()
test_uuids = UCI_HAPT.get_test_uuids()

tr_uuids = train_uuids[0:18]
vl_uuids = train_uuids[18:21]

(X_train,y_train) = UCI_HAPT.get_all_data_multi_thread(train_uuids,5)

#(X_vld,y_vld) = imu_bg.get_all_data(vl_uuids)
(X_test,y_test) = UCI_HAPT.get_all_data_multi_thread(test_uuids,5)

classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LAYING","TRANSITION"]




clf = Classifiers.HAPT_IMU_CNN(patience=1000,layers=3,kern_size=32,divide_kernel_size=True)
#uuids,epochs, val_data, imu_bg):
#clf.fit_gen(uuids=tr_uuids,epochs=10000,val_data=(X_vld,y_vld),imu_bg=imu_bg)
#clf.fit(X_train,y_train,X_test,y_test,batch_size=32,epochs=10000)
#class_weights = {0:1.,1:1.,2:1.,3:1.,4:1.,5:1.,6:10.}

y_train_inv = [ np.argmax(y) for y in y_train]
class_weights = class_weight.compute_class_weight('balanced',np.unique(y_train_inv),y_train_inv)
d_class_weights = dict(enumerate(class_weights))
clf.fit_class_weights(X_train,y_train,X_test,y_test,batch_size=32,class_weight=d_class_weights,epochs=10000)



'''
#Loading saved model
clf.loadBestWeights()
y_true = [ [np.argmax(y)] for y in y_test]#one_hot(labels_test)

y_predictions = clf.predict(X_test,batch_size=1)
y_predictions_inv = [ [np.argmax(y)] for y in y_predictions]

printClassificationReport(true=y_true,predictions=y_predictions_inv,classes=classes,filename="IMU_CNN_classification_report.txt")
plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="IMU_CNN_ConfusionMatrix_normalized.png")
printBalancedAccuracyScore(true=y_true,pred=y_predictions_inv,filename="IMU_CNN_balanced_accuracy.txt")
plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="IMU_CNN_ConfusionMatrix.png",normalize=False)
'''