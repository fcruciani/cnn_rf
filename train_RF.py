import numpy as np 
import pandas as pd 
import ES_Dataset as es
from multiprocessing import Pool as ThreadPool
import CNN_RF
import utils


#fold = "0"
print("Not for gpu use!!")
fold = input("Fold [0,1,2,3,4]: ")

train_uuids,test_uuids = es.get_train_test_uuids(fold,include_gyro=False)

#print("Training: ",train_uuids)
#print("Test: ",test_uuids)

print("Training A: ",len(train_uuids))
print("Test A: ",len(test_uuids))


train_uuids,test_uuids = es.get_train_test_uuids(fold,include_gyro=True)
print("Training A+G: ",len(train_uuids))
print("Test A+G: ",len(test_uuids))



X_train, y_train = es.get_all_dataset_IMU(train_uuids, show_progress=True,balanced=True,smote=True)
print("X: ",X_train.shape)
print("y: ",y_train.shape)


#X_test, y_test = es.get_all_dataset(test_uuids, show_progress=True,balanced=False)
#print("X balanced: ",X_test.shape)
#print("y balanced: ",y_test.shape)

nfilters = input("NUmber of filters? [12,24,48,96,128]:")
clf = CNN_RF.CNN_RF_Classifier(num_filters=int(nfilters))

print("Training RF")
clf.fit( X_train, y_train )
clf.save_classifier("./keras_logs/f"+fold+"_RF_"+nfilters+"filters_")

'''classes = ['Lying','Sitting','Walking','Running','Cycling']
print("Testing RF")
y_true = []
y_predictions_inv = []
#y_predictions_inv = [ [np.argmax(y)] for y in y_predictions]
for uuid in test_uuids:
	X_test, y_test = es.get_all_dataset([uuid], show_progress=True,balanced=False)
	y_pred = clf.predict(X_test)
	y_true.extend(y_test)
	y_predictions_inv.extend(y_pred)
	print( "Y true: ", np.unique(y_test)  )
	print( "Y pred: ", np.unique(y_pred)  )

utils.printClassificationReport(true=y_true,predictions=y_predictions_inv,classes=classes,filename="./results_paper/f"+fold+"_CNN_RF_classification_report.txt")
utils.plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/f"+fold+"_CNN_RF_ConfusionMatrix_normalized.png")
utils.printBalancedAccuracyScore(true=y_true,pred=y_predictions_inv,filename="./results_paper/f"+fold+"_CNN_RF_balanced_accuracy.txt")
utils.plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/f"+fold+"_CNN_RF_ConfusionMatrix.png",normalize=False)


save_pred_df = pd.DataFrame(columns=['true','pred'])
save_pred_df['true'] = pd.Series( [ x for x in y_true ] )
save_pred_df['pred'] = pd.Series( [ x for x in y_predictions_inv ] )
save_pred_df.to_csv("./results_paper/f"+fold+"_pred.csv",header=False,index=False)


y_true_idle = []
y_pred_idle = []

for y in y_true:
	if y == 0 or y == 1:
		y_true_idle.append(0)
	else:
		y_true_idle.append(y-1)

for y in y_predictions_inv:
	if y == 0 or y == 1:
		y_pred_idle.append(0)
	else:
		y_pred_idle.append(y-1)

classes = ['Idle','Walking','Running','Cycling']
utils.printClassificationReport(true=y_true_idle,predictions=y_pred_idle,classes=classes,filename="./results_paper/f"+fold+"_CNN_RF_classification_report_idle.txt")
utils.plotConfusionMatrix(true=y_true_idle,predictions=y_pred_idle,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/f"+fold+"_CNN_RF_ConfusionMatrix_normalized_idle.png")
utils.printBalancedAccuracyScore(true=y_true_idle,pred=y_pred_idle,filename="./results_paper/f"+fold+"_CNN_RF_balanced_accuracy_idle.txt")
utils.plotConfusionMatrix(true=y_true_idle,predictions=y_pred_idle,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/f"+fold+"_CNN_RF_ConfusionMatrix_idle.png",normalize=False)

save_pred_idle_df = pd.DataFrame(columns=['true','pred'])
save_pred_idle_df['true'] = pd.Series( [ x for x in y_true_idle ] )
save_pred_idle_df['pred'] = pd.Series( [ x for x in y_pred_idle ] )
save_pred_idle_df.to_csv("./results_paper/f"+fold+"_pred.csv",header=False,index=False)
'''