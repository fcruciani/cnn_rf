import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, balanced_accuracy_score
from sklearn.utils import class_weight
#local imports
import Classifiers
import HAPT_Dataset as UCI_HAPT
from HAPT_Dataset import ucihapt_datapath
from os.path import expanduser
import seaborn as sn

#get actual home path for current user
home = expanduser("~")
plt.rcParams.update({'font.size':11})

#Utility Functions
def printClassificationReport(predictions,true,classes,filename):
	cr = classification_report(np.array(true), np.array(predictions),target_names=classes,digits=4)
	print(cr)
	if not filename == "":
		with open(filename,"w") as out_file:
			out_file.write(cr)

def plotConfusionMatrix(predictions,true,classes,saveFig=True,showGraph=False,normalize=True,filename="undefined"):
		cm = confusion_matrix(np.array(true), np.array(predictions) )
		fig = plt.figure(figsize=(8, 7))
		if normalize:
			cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
		df_cm = pd.DataFrame(cm,index=classes,columns=classes)
		ax = sn.heatmap(df_cm,cmap='Blues',annot=True)
		plt.yticks(rotation=0)
		if showGraph:
			plt.show()
		if saveFig:
			fig.tight_layout()
			if filename == "undefined":
				fig.savefig(self.name+"_CM.png",dpi=300)
			else:
				fig.savefig(filename,dpi=300)

def printBalancedAccuracyScore(pred, true,filename=""):
		bal_acc = balanced_accuracy_score(np.array(true), np.array(pred))
		print("Balanced Accuracy: ",bal_acc)
		if not filename == "":
			with open(filename,"w") as out_file:
				out_file.write(str(bal_acc))



train_uuids = UCI_HAPT.get_train_uuids()
test_uuids = UCI_HAPT.get_test_uuids()

tr_uuids = train_uuids[0:18]
vl_uuids = train_uuids[18:21]

#(X_train,y_train) = UCI_HAPT.get_all_data(train_uuids)

#(X_vld,y_vld) = imu_bg.get_all_data(vl_uuids)
(X_test,y_test) = UCI_HAPT.get_all_data_multi_thread_noresampling(test_uuids,10)

classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LAYING","TRANSITION"]




clf = Classifiers.HAPT_IMU_CNN(patience=250,layers=3,kern_size=32,divide_kernel_size=True)

#Loading saved model
clf.loadBestWeights()
y_true = [ [np.argmax(y)] for y in y_test]#one_hot(labels_test)

y_predictions = clf.predict(X_test,batch_size=1)
y_predictions_inv = [ [np.argmax(y)] for y in y_predictions]

printClassificationReport(true=y_true,predictions=y_predictions_inv,classes=classes,filename="./results_cnn_hapt/IMU_CNN_classification_report.txt")
plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_cnn_hapt/IMU_CNN_ConfusionMatrix_normalized.png")
printBalancedAccuracyScore(true=y_true,pred=y_predictions_inv,filename="./results_cnn_hapt/IMU_CNN_balanced_accuracy.txt")
plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_cnn_hapt/IMU_CNN_ConfusionMatrix.png",normalize=False)
