import numpy as np 
import pandas as pd 
import utils


folds = ["0","1","2","3","4"]


nfilters = input("Number of filters? [12,24,48,96,128]:")

all_preds_df = pd.DataFrame(columns=['true','pred']) 
all_preds_idle_df = pd.DataFrame(columns=['true','pred']) 

for fold in folds:
	pred_df = pd.read_csv( "./results_paper/f"+fold+"_pred_"+str(nfilters)+"filters.csv", names=['true','pred'] )
	all_preds_df = pd.concat([all_preds_df,pred_df], ignore_index=True)

	pred_idle_df = pd.read_csv( "./results_paper/f"+fold+"_pred_idle_"+str(nfilters)+"filters.csv", names=['true','pred'] )
	all_preds_idle_df = pd.concat([all_preds_idle_df,pred_idle_df], ignore_index=True)


y_true = all_preds_df['true'].values
y_predictions_inv = all_preds_df['pred'].values

classes = ['Lying','Sitting','Walking','Running','Cycling']
utils.printClassificationReport(true=y_true,predictions=y_predictions_inv,classes=classes,filename="./results_paper/5fold_CNN_RF_classification_report_"+str(nfilters)+"filters.txt")
utils.plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/5fold_CNN_RF_ConfusionMatrix_normalized_"+str(nfilters)+"filters.png")
utils.printBalancedAccuracyScore(true=y_true,pred=y_predictions_inv,filename="./results_paper/5fold_CNN_RF_balanced_accuracy_"+str(nfilters)+"filters.txt")
utils.plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/5fold_CNN_RF_ConfusionMatrix_"+str(nfilters)+"filters.png",normalize=False)


y_true_idle = all_preds_idle_df['true'].values
y_pred_idle = all_preds_idle_df['pred'].values

classes = ['Idle','Walking','Running','Cycling']
utils.printClassificationReport(true=y_true_idle,predictions=y_pred_idle,classes=classes,filename="./results_paper/5fold_CNN_RF_classification_report_idle_"+str(nfilters)+"filters.txt")
utils.plotConfusionMatrix(true=y_true_idle,predictions=y_pred_idle,classes=classes,showGraph=False,w=5,h=4,saveFig=True,filename="./results_paper/5fold_CNN_RF_ConfusionMatrix_normalized_idle_"+str(nfilters)+"filters.png")
utils.printBalancedAccuracyScore(true=y_true_idle,pred=y_pred_idle,filename="./results_paper/5fold_CNN_RF_balanced_accuracy_idle_"+str(nfilters)+"filters.txt")
utils.plotConfusionMatrix(true=y_true_idle,predictions=y_pred_idle,classes=classes,showGraph=False,w=5,h=4,saveFig=True,filename="./results_paper/5fold_CNN_RF_ConfusionMatrix_idle_"+str(nfilters)+"filters.png",normalize=False)
