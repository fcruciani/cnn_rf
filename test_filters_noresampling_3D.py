import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, accuracy_score
from sklearn.utils import class_weight
#local imports
import Classifiers
import HAPT_Dataset as UCI_HAPT
from HAPT_Dataset import ucihapt_datapath
from os.path import expanduser
import utils

#get actual home path for current user
home = expanduser("~")




train_uuids = UCI_HAPT.get_train_uuids()
test_uuids = UCI_HAPT.get_test_uuids()

#tr_uuids = train_uuids[0:18]
#vl_uuids = train_uuids[18:21]

#(X_train,y_train) = UCI_HAPT.get_all_data_multi_thread_noresampling_3D(train_uuids,3)

#(X_vld,y_vld) = imu_bg.get_all_data(vl_uuids)
(X_test,y_test) = UCI_HAPT.get_all_data_multi_thread_noresampling_3D(test_uuids,3)

classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LYING DOWN","TRANSITION"]

sFilters = input("Number of filters e.g. 12-24-48-96-128:")

if sFilters not in ["12","24","48","96","128"]:
	input("Not valid number fo filters")

clf = Classifiers.IMU_CNN_3D_FEATURE_EXTRACTOR(patience=2500,num_filters=int(sFilters),layers=3,kern_size=32,divide_kernel_size=True,suffix="50Hz")


#Loading saved model
clf.loadBestWeights()
y_true = [ [np.argmax(y)] for y in y_test]#one_hot(labels_test)

y_predictions = clf.predict(X_test,batch_size=1)
y_predictions_inv = [ [np.argmax(y)] for y in y_predictions]

utils.printClassificationReport(true=y_true,predictions=y_predictions_inv,classes=classes,filename="./results_paper/IMU_3D_CNN"+sFilters+"_filters_classification_report.txt")
utils.plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/IMU_3D_CNN"+sFilters+"_filters_ConfusionMatrix_normalized.png")
utils.printBalancedAccuracyScore(true=y_true,pred=y_predictions_inv,filename="./results_paper/IMU_3D_CNN"+sFilters+"_filters_balanced_accuracy.txt")
utils.plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="./results_paper/IMU_3D_CNN"+sFilters+"_filters_ConfusionMatrix.png",normalize=False)
