#!/usr/bin/bash
#SBATCH -N 1
#SBATCH -p shareq
#SBATCH -n 21
#SBATCH --mem 200G
#SBATCH -t 08:10:00
#SBATCH --gres=gpu:1
#SBATCH --job-name=CNN_layers
#SBATCH --output=output_layers.txt
#SBATCH --error=error_layers.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=f.cruciani@ulster.ac.uk

module purge
module load shared
module load cm-ml-python3deps/2.3.2 
module load ml-python3/tensorflow/1.14.0 
module load ml-python3/keras/2.2.4 
module load openmpi/cuda/64/3.1.4
module load hpcx/2.4.0
module list
#layer k filters

python3.6 final_train_CNN_noresampling_3D.py 1 2 12
python3.6 final_train_CNN_noresampling_3D.py 2 2 12
python3.6 final_train_CNN_noresampling_3D.py 3 2 12
python3.6 final_train_CNN_noresampling_3D.py 4 2 12
python3.6 final_train_CNN_noresampling_3D.py 1 8 12
python3.6 final_train_CNN_noresampling_3D.py 2 8 12
python3.6 final_train_CNN_noresampling_3D.py 3 8 12
python3.6 final_train_CNN_noresampling_3D.py 4 8 12
