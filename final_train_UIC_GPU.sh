#!/usr/bin/bash
#SBATCH -N 1
#SBATCH -p shareq
#SBATCH -n 20
#SBATCH --mem 20G
#SBATCH -t 08:10:00
#SBATCH --gres=gpu:1
#SBATCH --job-name=HAR_UIC
#SBATCH --output=output_UIC.txt
#SBATCH --error=error_UIC.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=f.cruciani@ulster.ac.uk

module purge
module load shared
module load cm-ml-python3deps/2.3.2 
module load ml-python3/tensorflow/1.14.0 
module load ml-python3/keras/2.2.4 
module load openmpi/cuda/64/3.1.4
module load hpcx/2.4.0
module list

python3.6 final_train_UIC_noresampling_3D.py 3 8 24
python3.6 final_train_UIC_noresampling_3D.py 3 16 24
python3.6 final_train_UIC_noresampling_3D.py 3 32 24
python3.6 final_train_UIC_noresampling_3D.py 3 64 24

python3.6 final_train_UIC_noresampling_3D.py 3 8 48
python3.6 final_train_UIC_noresampling_3D.py 3 16 48
python3.6 final_train_UIC_noresampling_3D.py 3 32 48
python3.6 final_train_UIC_noresampling_3D.py 3 64 48
